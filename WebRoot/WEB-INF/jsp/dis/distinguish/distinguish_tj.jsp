﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<base href="<%=basePath%>">

<!-- jsp文件头和头部 -->
<%@ include file="../../system/index/top.jsp"%>
<!-- 百度echarts -->
<script src="plugins/echarts/echarts.min.js"></script>
<script type="text/javascript">
setTimeout("top.hangge()",500);
</script>
</head>
<body class="no-skin">

	<!-- /section:basics/navbar.layout -->
	<div class="main-container" id="main-container">
		<!-- /section:basics/sidebar -->
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="hr hr-18 dotted hr-double"></div>
					<div class="row">
						<div class="col-xs-12">
						<div class="span6">
							<div class="tabbable">
						            <ul class="nav nav-tabs" id="myTab">
						              <li class="active"><a data-toggle="tab" href="#home"><i class="green icon-home bigger-110"></i>柱状图</a></li>
						              <li><a data-toggle="tab" href="#profile"><i class="green icon-cog bigger-110"></i>折线图</a></li>
						            </ul>
						            <div class="tab-content">
									  <div id="home" class="tab-pane in active">
									  	<div id="main" style="width: 600px;height:300px;"></div>
									  </div>
									  <div id="profile" class="tab-pane">
									  	<div id="main2" style="width: 600px;height:300px;"></div>
									  </div>
									</div>
						    </div>
						</div>
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</div>
				<!-- /.page-content -->
			</div>
		</div>
		<!-- /.main-content -->


		<!-- 返回顶部 -->
		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
			<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
		</a>

	</div>
	<!-- /.main-container -->

	<!-- basic scripts -->
	<!-- 页面底部js¨ -->
	<%@ include file="../../system/index/foot.jsp"%>
	<!-- ace scripts -->
	<script src="static/ace/js/ace/ace.js"></script>
	<!-- inline scripts related to this page -->
	<script type="text/javascript">
		$(top.hangge());
	</script>
	<script type="text/javascript">
	
		function getTj(){
			$.ajax({
				type: "POST",
				url: '<%=basePath%>distinguish/getTj.do',
		    	data: {tm:new Date().getTime()},
				dataType:"json",
				success: function(data){
					
					for (var i = 0; i < data.varList.length; i++) {
						arrData1[i] = data.varList[i].USERNAME;
						arrData2[i] = Number(data.varList[i].UC);
						if(i % 2 == 0){
							arrColor[i] = "#6FB3E0";
						}else{
							arrColor[i] = "#87B87F";
						}
					}
					 
					myChart.setOption(option);
					myChart2.setOption(option2);
	              }
			});
		}
	
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('main'));
        var myChart2 = echarts.init(document.getElementById('main2'));

        // 指定图表的配置项和数据
        var arrData1 = [];
        var arrData2 = [];
        var arrColor = [];
		var option = {
            title: {
                text: '用户使用统计'
            },
            tooltip: {},
            xAxis: {
                data: arrData1
            },
            yAxis: {},
            series: [
               {
                name: '',
                type: 'bar',
                data: arrData2,
                itemStyle: {
                    normal: {
                        color: function(params) {
                            // build a color map as your need.
                            var colorList = arrColor;
                            return colorList[params.dataIndex];
                        }
                    }
                }
               }
            ]
        };
		
		var option2 = {
	            title: {
	                text: '用户使用统计'
	            },
	            tooltip: {},
	            xAxis: {
	                data: arrData1
	            },
	            yAxis: {},
	            series: [
	               {
	                name: '',
	                type: 'line',
	                data: arrData2,
	                itemStyle: {
	                    normal: {
	                        color: function(params) {
	                            // build a color map as your need.
	                            var colorList = arrColor;
	                            return colorList[params.dataIndex];
	                        }
	                    }
	                }
	               }
	            ]
	        };	

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
        myChart2.setOption(option2);
        getTj();
	</script>
<script type="text/javascript" src="static/ace/js/jquery.js"></script>
</body>
</html>