<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
		+ request.getServerName() + ":" + request.getServerPort()
		+ path + "/";
%>
<!DOCTYPE html>
<html lang="en">

<head>
<base href="<%=basePath%>">
<title>文章识别系统</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<link rel="stylesheet" href="static/login/bootstrap.min.css" />
<link rel="stylesheet" href="static/login/css/camera.css" />
<link rel="stylesheet" href="static/login/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="static/login/matrix-login.css" />
<link href="static/login/font-awesome.css" rel="stylesheet" />


</head>
<body>

	<div style="width:100%;text-align: center;margin: 0 auto;position: absolute;">
		<!-- 登录 -->
		<div id="windows1">
		<div id="loginbox" >
			<form action="" method="post" name="loginForm" id="loginForm">
				<div class="control-group normal_text">
					<h3>
						手写汉字文章识别系统
					</h3>
				</div>
				<div class="control-group">
					<table id="simple-table" class="table table-striped table-bordered table-hover"  style="width: 100%;text-align: center;">	
					<tr>
						<td style="width:50%;text-align: center;">
							<c:if test="${null != pd.IMGPATH}">
								<img src="<%=path %>/${pd.IMGPATH}" width="410px;" />
							</c:if>
						</td>
						<td>
							<textarea style="width:90%;height: 250px;" rows="" cols="" placeholder="显示结果" >${pd.CONTENT}</textarea>
						</td>
					</tr>
					</table>
				</div>
				<div style="float:right;padding-right:10%;">
				</div>
				<div class="form-actions">
					<div style="width:86%;padding-left:8%;">
						<span class="pull-right" style="padding-right:3%;"><a href="https://www.baidu.com/s?ie=utf-8&f=8&rsv_bp=1&ch=&tn=baiduerr&bar=&wd=${pd.CONTENT}" target="_blank" class="btn btn-success">搜索</a></span>
						<span class="pull-right" style="padding-right:3%;"><a href="javascript:getOut('${pd.DISTINGUISH_ID}');" class="btn btn-success">导出</a></span>
						<span class="pull-right"><a onclick="add();" class="flip-link btn btn-info" id="to-recover">上传图片识别</a></span>
					</div>
				</div>
			</form>
		</div>
			<div>
				<table id="simple-table" class="table table-striped table-bordered table-hover"  style="width: 100%;text-align: center;">	
					<!-- 开始循环 -->	
					<c:forEach items="${varList}" var="var" varStatus="vs">
						<tr>
							<td style="width: 100%;text-align: center;">
								<img src="<%=path %>/${var.IMGPATH}">
								${var.TITLE}
							</td>
						</tr>
					</c:forEach>
					<tr class="main_info">
						<td style="width: 100%;text-align: center;">
							<img src="<%=path %>/uploadFiles/uploadImgs/appds.png" />
						</td>
					</tr>
				</table>
			</div>
		</div>
		
	</div>
	
	<script src="static/js/jquery-1.7.2.js"></script>
	<script type="text/javascript" src="static/js/jquery.tips.js"></script>
	
	<!--引入弹窗组件2start-->
	<script type="text/javascript" src="plugins/attention/drag/drag.js"></script>
	<script type="text/javascript" src="plugins/attention/drag/dialog.js"></script>
	<link type="text/css" rel="stylesheet" href="plugins/attention/drag/style.css"  />
	<!--引入弹窗组件2end-->
	
	<script type="text/javascript">
	
	//新增
	function add(){
		 var diag = new Dialog();
		 diag.Drag=true;
		 diag.Title ="导入";
		 diag.URL = '<%=path%>/appImg/goAdd.do';
		 diag.Width = 500;
		 diag.Height = 300;
		 diag.Modal = true;				//有无遮罩窗口
		 diag. ShowMaxButton = true;	//最大化按钮
	     diag.ShowMinButton = true;		//最小化按钮
		 diag.CancelEvent = function(){ //关闭事件
			 if(diag.innerFrame.contentWindow.document.getElementById('zhongxin').style.display == 'none'){
				 window.location.reload();
			}
			diag.close();
		 };
		 diag.show();
	}
	
	//导出
	function getOut(Id){
		if('' != Id){
			window.location.href = "<%=basePath%>appImg/getOut.do?DISTINGUISH_ID="+Id;
		}
	}
	
	</script>
	
</body>

</html>