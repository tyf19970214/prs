package com.fh.controller.dis.distinguish;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import org.apache.shiro.session.Session;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.fh.controller.base.BaseController;
import com.fh.service.dis.advertisement.AdvertisementManager;
import com.fh.service.dis.distinguish.DistinguishManager;
import com.fh.util.Const;
import com.fh.util.DateUtil;
import com.fh.util.DelAllFile;
import com.fh.util.FileDownload;
import com.fh.util.FileUpload;
import com.fh.util.FileZip;
import com.fh.util.Freemarker;
import com.fh.util.Jurisdiction;
import com.fh.util.PageData;
import com.fh.util.PathUtil;
import com.fh.util.Tools;

@Controller
@RequestMapping(value="/appImg")
public class imgController extends BaseController {
	
	@Resource(name="distinguishService")
	private DistinguishManager distinguishService;
	@Resource(name="advertisementService")
	private AdvertisementManager advertisementService;
	
	/**展示的页面
	 * @param
	 * @throws Exception
	 */
	@RequestMapping(value="/show")
	public ModelAndView tj() throws Exception{
		ModelAndView mv = this.getModelAndView();
		PageData pd = new PageData();
		pd = this.getPageData();
		Session session = Jurisdiction.getSession();
		Object o = session.getAttribute("DISTINGUISH_ID");
		if(null != o){
			pd.put("DISTINGUISH_ID", (String)o);
			pd = distinguishService.findById(pd);	//根据ID读取
			pd.put("IMGPATH", (String)session.getAttribute("IMGPATH"));
		}
		//显示广告
		pd.put("NTIME",  DateUtil.getDay());
		List<PageData>	varList = advertisementService.listAll(pd);	//列出Advertisement列表
		mv.addObject("varList", varList);
		mv.addObject("pd", pd);
		mv.setViewName("dis/distinguish/show");
		return mv;
	}
	
	/**去新增页面
	 * @param
	 * @throws Exception
	 */
	@RequestMapping(value="/goAdd")
	public ModelAndView goAdd()throws Exception{
		ModelAndView mv = this.getModelAndView();
		PageData pd = new PageData();
		pd = this.getPageData();
		mv.setViewName("dis/distinguish/show_edit");
		mv.addObject("msg", "save");
		mv.addObject("pd", pd);
		return mv;
	}
	
	/**保存
	 * @param
	 * @throws Exception
	 */
	@RequestMapping(value="/save")
	public ModelAndView save(
			@RequestParam(value="tp",required=false) MultipartFile file
			) throws Exception{
		ModelAndView mv = this.getModelAndView();
		PageData pd = new PageData();
		pd = this.getPageData();
		String  ffile = DateUtil.getDays(), fileName = "";
		if (null != file && !file.isEmpty()) {
			String filePath = PathUtil.getClasspath() + Const.FILEPATHIMG + ffile;		//文件上传路径
			fileName = FileUpload.fileUp(file, filePath, this.get32UUID());				//执行上传
			String DISTINGUISH_ID = this.get32UUID();
			pd.put("DISTINGUISH_ID", DISTINGUISH_ID);	//主键
			pd.put("CTIME", Tools.date2Str(new Date()));	//时间
			pd.put("CONTENT", getContent(filePath + "/" + fileName));	//内容
			pd.put("USERNAME", "游客"); 
			distinguishService.save(pd);
			Session session = Jurisdiction.getSession();
			session.setAttribute("DISTINGUISH_ID", DISTINGUISH_ID);
			session.setAttribute("IMGPATH", Const.FILEPATHIMG + ffile + "/"+ fileName);
		}else{
			System.out.println("上传失败");
		}
		mv.addObject("msg","success");
		mv.setViewName("save_result");
		return mv;
	}
	
	/**获取图片里面的文字
	 * @param ffile
	 * @return
	 * @throws TesseractException
	 */
	public String getContent(String ffile) throws TesseractException {
		 ITesseract instance = new Tesseract();
		 	instance.setDatapath(PathUtil.getClassResources());
	        instance.setLanguage("chi_sim");
	        File imgDir = new File(ffile);
            ImageIO.scanForPlugins();
	        String ocrResult = instance.doOCR(imgDir);
	        return ocrResult;
	}
	
	 /**导出
	 * @param
	 * @throws Exception
	 */
	@RequestMapping(value="/getOut")
	public void getOut(HttpServletResponse response)throws Exception{
		Map<String,Object> root = new HashMap<String,Object>();
		PageData pd = new PageData();
		pd = this.getPageData();
		pd = distinguishService.findById(pd);	//根据ID读取
		root.put("CONTENT", pd.getString("CONTENT"));
		DelAllFile.delFolder(PathUtil.getClasspath()+"admin/ftl"); //生成代码前,先清空之前生成的代码
		String filePath = "admin/ftl/code/";						//存放路径
		String ftlPath = "createCode";								//ftl路径
		Freemarker.printFile("txt.ftl", root, "content.txt", filePath, ftlPath);
		if(FileZip.zip(PathUtil.getClasspath()+"admin/ftl/code", PathUtil.getClasspath()+"admin/ftl/txt.zip")){
			/*下载代码*/
			FileDownload.fileDownload(response, PathUtil.getClasspath()+"admin/ftl/txt.zip", "txt.zip");
		}
	}
	
}
	
 