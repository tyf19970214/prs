package com.fh.util;

import java.util.UUID;

/**
 * 说明：UUID
 * 作者：FH Admin fh313596790qq
 * 官网：www.fhadmin.org
 */
public class UuidUtil {

	public static String get32UUID() {
		String uuid = UUID.randomUUID().toString().trim().replaceAll("-", "");
		return uuid;
	}
	public static void main(String[] args) {
		System.out.println(get32UUID());
	}
}

